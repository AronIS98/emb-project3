#include <avr/io.h>
#include "digital_out.h"
// class Digital_out {        // The class
    

// };

Digital_out::Digital_out (uint8_t a) { 
    pinMask = a;         // Access specifier
}
void Digital_out::init() {  // Method/function defined inside the class
    DDRB |= 1<<pinMask;
    };
  
void Digital_out::set_hi() {  // Method/function defined inside the class
    PORTB |= 1<<pinMask;
    }

void Digital_out::set_lo() {  // Method/function defined inside the class
    PORTB &= ~(1<<pinMask);
    }


void Digital_out::toggle() {  // Method/function defined inside the class
    PORTB ^= 1<<pinMask;
    }
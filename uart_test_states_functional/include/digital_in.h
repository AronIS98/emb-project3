#include <stdint.h>
#ifndef DIGITAL_IN_H
#define DIGITAL_IN_H

class Digital_in
{

    public:
        Digital_in(uint8_t);
        uint8_t pinMask;
        void init();
        bool is_hi();
        bool is_lo();
};


#endif
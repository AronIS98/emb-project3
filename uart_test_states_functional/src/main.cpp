

#include <stdint.h>
#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include "uart_hall.h"
// #include <avr/interrupt.h>
// #include <util/delay.h>
#include "digital_out.h"
#include "digital_in.h"
#include "encoder.h"
// #include <Arduino.h>
#include "controler.h"

int command = 0; // for incoming serial data
char state = 'r';
int state_timer = 0;

volatile int enc_position = 0;
volatile int speed_counter = 0;
volatile int speed_timer = 10;
volatile double speed = 1.0;
int desired_speed = 600;
int timer_counter;
int curr_pwm;
Digital_out led(5);
Digital_out motor(1);
Encoder enc;
Controler contr;


volatile int g_time;
int isr_count = 30;

// char TxBuffer[32];
// int indx, len;


const uint8_t counter = 0x10; //0b00010000
int main(void)
{

  PIControler PI;
   curr_pwm = PI.update(desired_speed,speed);

	// const uint8_t start[] = "Program Start\n\r";
  char start[] = "Program Start\n\r";
  char state_info = 'r';
	uint8_t data = 'A';
  DDRD |= 1<<PD1;
  DDRB |= 1<<PB0;
  DDRD |= 1 << 6;
  DDRD |= 1 <<PD7;
  EICRA |= (1 << ISC10) | (1 << ISC11); // set INT0 to trigger on RISING edge.
  EIMSK |= (1 << INT1);
  DDRD &= ~(1 << 4); // set the PD3 pin as input
  enc.pwm_update(curr_pwm);



  // // Setup for state of onboard led
  // DDRB |= PORTB5;

  TCCR2A = 0;           // normal mode, OC2x pin disconnected
  TCCR2B = 0x07;        // 1024x prescale

  TIMSK2 = (1<<TOIE2);  // enable overflow interrupt


  // // Interupt for led blinking
  // timer_counter = 10000; //34286;   // preload timer 65536-16MHz/256/2Hz
  // TCNT2 = timer_counter;  // preload timer
  // TCCR2B |= (1 << CS12);  // 256 prescaler 
  // TIMSK2 |= (1 << TOIE1);   // enable timer overflow interrupt
  // // TCNT1 = timer_counter;   // preload timer
  // // TCCR1B |= (1 << CS12);    // 256 prescaler 
  // // TIMSK1 |= (1 << TOIE1);  


	// DDRD |= 0xF0; //0b11110000
	uart_init(9600,0);

	sei();
	uart_send_string(start);

    while (1) 
    {
      // _delay_ms(100);
		
		if(uart_read_count() > 0){
			data = uart_read();
			
      command = data;
      uart_send_string("State changed from: ");
      uart_send_byte(state_info);
      uart_send_string(" to ");
      uart_send_byte(data);
      // uart_send_string(data);
      uart_send_byte('\n');


		}

    switch (state)
    {
    case 'p':
    sei();
    
      if ((command == 'p') && state_info != 'p'){
        state = 'p';
      }
      if (command == 'o'){
        state = 'o';
      }
      if (command == 's'){
        state = 's';
      }
      if (command == 'r'){
        state = 'r';
      }
      if (command == 'k'){

      }

      if(state_info != 'p'){
        isr_count = 61;
        }
      state_info = 'p';

      break;

    case 'o':
    sei();
      if (command == 'p'){
        state = 'p';
      }
      if (command == 'o'){
        state = 'o';
      }
      if (command == 's'){
        state = 's';
      }
      if (command == 'r'){
        state = 'r';
      }
      PORTD |= 1<<PD7;
      PORTB &= ~1<<PB0;
      curr_pwm = PI.update(desired_speed,speed);

      enc.pwm_update(curr_pwm);
      
      state_info = 'o';
      led.set_hi();

      break;
    
    case 's':
    // sei();
      if (command == 'p'){
        state = 'p';
      }
      if (command == 'o'){
        state = 'o';
      }
      if (command == 's'){
        state = 's';
      }
      if (command == 'r'){
        state = 'r';
      }
      PORTD &= ~(1<<PD7);
      if(state_info != 's'){
      isr_count = 15;  
      }
      state_info = 's';
   
      break;

    case 'r':
      

      // cli();
      led.set_lo();
      PORTD |= 1<<PD7;
      PORTB &= ~1<<PB0;

      enc.timer_msec(speed_timer);
      enc.init();
      led.set_lo();
      motor.set_lo();

      enc.pwm_init();

      state_info = 'r';
      uart_send_string("State has been initialized: Ready to receive commands\n");
      command = 'p';
      state = 'p';
      state_info = 'p';
      isr_count = 30;
      
      break;
    }
}

}

ISR(INT0_vect)
{

  DDRD &= ~(1<<4);
  if(((PIND>>4) & 1))
  {
    enc_position--;
    speed_counter--;
  }
  else
  {
    enc_position++;
    speed_counter++;
  }
}

ISR(TIMER1_COMPA_vect)
{
  speed = ((speed_counter*1.0)*1000.0)/((speed_timer*1.0));
  speed_counter = 0;

}

ISR(TIMER0_COMPA_vect)
{
  
  PORTD |= 1 << 6;
}

ISR(TIMER0_COMPB_vect)
{
  PORTD &= ~(1 << 6);
}

ISR(TIMER2_OVF_vect)
{
if(!g_time)
    led.toggle();

g_time++;
if( g_time >= isr_count ){

    g_time = 0;
}
}

ISR(INT1_vect){
  state = 's';
  command = 's';
}